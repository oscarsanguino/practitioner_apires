#partimos de una imagen node
FROM node

#crear workspace
WORKDIR /apitechu

#incluir todo en el workspace creado
ADD . /apitechu

#abrimos puerto 3000
EXPOSE 3000

#ejecutar npm start
CMD ["npm", "start"]
