var express = require('express');
var app = express();
var bodyParser = require('body-parser');
app.use(bodyParser.json());

var baseMLabURL = "https://api.mlab.com/api/1/databases/apitechu_sanguino/collections/";
var mLabAPIKey = "apiKey=NghVFC8ETRbzWYngEQCplqVuJoXRyTsd";
var requestJson = require('request-json');

var port = process.env.PORT || 3000;
app.listen(port);
console.log("API escuchando en caliente en el puerto " + port);

var USERS_FILE = './mocks/users.json';
var users = require(USERS_FILE);

  function writeUserDataToFile(data){
    var fs = require('fs');
    var jsonUserData = JSON.stringify(data);

    fs.writeFile(USERS_FILE, jsonUserData, "utf8",
      function(err){
        var msg;
        if (err){
          console.log(err);
        }
        else {
          console.log("Datos escritos en archivo");
        }

      }
    );
  }

  app.post("/apitechu/v1/login",
    function(req, res){

      console.log("POST /apitechu/v1/login");

      var email = req.body.email;
      var password = req.body.password;
      console.log("params: " + email + " - " + password);

      var userFound = null;

      for (user of users){
        if (user.email == email){
          userFound = user;
          break;
        }
      }

      var message = null;

      if (userFound == null){
        console.log("invalid email");
        message = {"mensaje" : "Email no existe."};
      }
      else if (userFound.password != password){
        console.log("invalid password");
        message = {"mensaje" : "Password incorrecta"};
      }
      else {
        userFound.logged = true;
        writeUserDataToFile(users);
        console.log("user successfully logged!");
        message = {"mensaje" : "Login correcto","idUsuario" : user.id};
      }
      res.send(message);
    }
  )//end post function


  app.post("/apitechu/v1/logout",
    function(req, res){

      console.log("POST /apitechu/v1/login");

      var id = req.body.id;
      var password = req.body.password;
      console.log("params: " + id);

      var userFound = null;

      for (user of users){
        if (user.id == id){
          userFound = user;
          break;
        }
      }

      var message = null;

      if (userFound == null){
        console.log("invalid id");
        message = {"mensaje" : "id no existe."};
      }
      else if (!userFound.logged){
        console.log("user is not looged");
        message = {"mensaje" : "el usuario no estaba logado"};
      }
      else {
        delete userFound.logged;
        message = {"mensaje" : "logout correcto. Bye!", id:userFound.id};
      }

      res.send(message);
    }
  );




  app.get("/apitechu/v1",
    function(request, response){
      console.log("GET /apitechu/v1");
      response.send({msg:"hola desde apitechu"});
    }
  )


  app.get("/apitechu/v1/users",
    function(req, res){
        console.log("GET /apitechu/v1/users");

        //res.sendFile('./usuarios.json'); //DEPRECATED
        //res.sendFile('usuarios.json', {root: __dirname});

        var users = require('./mocks/users.json');
        res.send(users);
    }
  );


  /*
  app.post("/apitechu/v1/users",
    function(req, res){
      console.log("POST /apitechu/v1/users");
      console.log(req.body);

      var newUser = {
        first_name: req.body.first_name,
        last_name: req.body.last_name,
        country: req.body.country
      };

      console.log(newUser);

      var users = require('./usuarios.json');
      users.push(newUser);
      writeUserDataToFile(users);

      var msg = "Usuario guardado con éxito";
      console.log(msg);
      res.send({msg: msg});
    }
  ) //end app.POST

  app.delete("/apitechu/v1/users/:id",
    function(req, res){
        console.log("DELETE /apitechu/v1/users");
        console.log(req.params);
        console.log(req.params.id);

        var users = require('./usuarios.json');
        console.log("users length before splice: " + users.length);
        users.splice(req.params.id -1, 1);
        console.log("users length after splice: " + users.length);
        writeUserDataToFile(users);

        var msg = "Usuario borrado";
        console.log(msg);
        res.send({msg: msg})
    }
  )



  app.post("/apitechu/v1/monstruo/:p1/:p2",
    function(req, res){
        console.log("Parámetros");
        console.log(req.params);

        console.log("Query string");
        console.log(req.query);

        console.log("Body");
        console.log(req.body);

        console.log("Headers");
        console.log(req.headers);


    });
    */


    /***********************************************
                            V2
    ***********************************************/
    app.get("/apitechu/v2/users",
      function(req, res){
          console.log("GET /apitechu/v2/users");

          var httpClient = requestJson.createClient(baseMLabURL);
          console.log("Cliente HTTP creado");

          httpClient.get("user?" + mLabAPIKey,
            function(err, resMLab, body){
              var response = !err ? body : {
                "msg": "Error obteniendo usuarios"
              }
              res.send(response);
            }
          );
      }
    );


    app.get("/apitechu/v2/users/:id",
      function(req, res){
          console.log("GET /apitechu/v2/users/:id");

          var id = req.params.id;
          var query = '&q={"id":' + id + '}';

          var httpClient = requestJson.createClient(baseMLabURL);
          console.log("Cliente HTTP creado");

          httpClient.get("user?" + mLabAPIKey + query,
            function(err, resMLab, body){

              var response = {};
              if (err){
                response = {
                  "msg": "Error obteniendo usuario."
                }
                res.status(500);
              }
              else {
                if (body.length > 0){
                  response = body;
                  //por defecto se devuelve status 200
                }
                else {
                  response = {
                    "msg": "Usuario no encontrado"
                  }
                  res.status(404);
                }
              }
              res.send(response);
            }
          );
      }
    );


    app.put("/apitechu/v2/login",
      function(req, res){

        console.log("POST /apitechu/v2/login");

        var email = req.body.email;
        var password = req.body.password;
        console.log("params: " + email + " - " + password);

        var response = null;

        if (!email || !password){
          console.log("email o password no válidos.");
          response = {
            msg: "email o password no válidos"
          }
          res.status(401);
          res.send(response);
          return;
        }

        var query = '&q={"email":"' + email + '", "password":"' + password + '"}';

        var httpClient = requestJson.createClient(baseMLabURL);
        console.log("Cliente HTTP creado");

        console.log("user?" + mLabAPIKey + query);

        httpClient.get("user?" + mLabAPIKey + query,
          function(err, resMLab, body){

            var response = {};
            if (err){
              response = {
                "msg": "Error obteniendo usuario."
              }
              res.status(500);
            }
            else {
              if (body.length > 0){
                console.log(body);

                //actualizamos el usuario añadiendo logged: true
                var putbody = '{"$set":{"logged":true}}';

                httpClient.put("user?" + mLabAPIKey + query,
                  JSON.parse(putbody),
                  function(errPut, resMLabPut, bodyPut){
                    console.log("login put callback")
                  }
                );//end put

                //devolvemos el usuario con el cambio
                body[0].logged = true;
                response = body;
                //por defecto se devuelve status 200
              }
              else {
                response = {
                  "msg": "combinación email - password no válida"
                }
                res.status(401);
              }
            }
            res.send(response);
          }
        );
      }
    )//end login



    app.put("/apitechu/v2/logout/:id",
      function(req, res){

        console.log("POST /apitechu/v2/logout/:id");

        var id = req.params.id;
        console.log("params: " + id);

        var response = null;

        if (!id){
          console.log("id no válidos.");
          response = {
            msg: "id no válido"
          }
          res.status(401);
          res.send(response);
          return;
        }

        var query = '&q={"id":' + id + '}';

        var httpClient = requestJson.createClient(baseMLabURL);
        console.log("Cliente HTTP creado");

        console.log("user?" + mLabAPIKey + query);

        httpClient.get("user?" + mLabAPIKey + query,
          function(err, resMLab, body){

            var response = {};
            if (err){
              response = {
                "msg": "Error obteniendo usuario."
              }
              res.status(500);
            }
            else {
              if (body.length > 0){
                console.log(body);

                //actualizamos el usuario quitando la propiedad logged
                var putbody = '{"$unset":{"logged": ""}}';

                httpClient.put("user?" + mLabAPIKey + query,
                  JSON.parse(putbody),
                  function(errPut, resMLabPut, bodyPut){
                    console.log("logout put callback")
                  }
                );//end put

                //devolvemos el usuario con el cambio
                delete body[0].logged;
                response = body;
                //por defecto se devuelve status 200
              }
              else {
                response = {
                  "msg": "combinación email - password no válida"
                }
                res.status(404);
              }
            }
            res.send(response);
          }
        );
      }
    )//end login


    app.get("/apitechu/v2/users/:userid/accounts",
      function(req, res){
          console.log("GET /apitechu/v2/users/:userid/accounts");

          var userid = req.params.userid;
          var query = '&q={"userid":' + userid + '}';

          var httpClient = requestJson.createClient(baseMLabURL);
          console.log("Cliente HTTP creado");

          httpClient.get("accounts?" + mLabAPIKey + query,
            function(err, resMLab, body){
              var response = !err ? body : {
                "msg": "Error obteniendo usuarios"
              }
              res.send(response);
            }
          );
      }
    );
